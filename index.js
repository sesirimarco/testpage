let path = './';
let http = require('http');
let express = require('express');
let app = express();
let port = process.env.PORT || 80
app.use(express.static(__dirname + '/public'));

http.createServer(app).listen(port);